#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "mergesort.h"
#include <errno.h>



//Sort functions

char *trim(char *str)
{
    int start = 0;
    int end = strlen(str)-1;
    int hasQuote = 0;
    if(str[strlen(str)-1] == '\"')
    {
        hasQuote = 1;
        end--;
    }
    while(start < strlen(str))
    {
        if(isspace(str[start]))
        {
            start++;
        }
        else
        {
            break;
        }
    }
    if(start == strlen(str))
    {
        return str;
    }
    while(end>0)
    {
        if(isspace(str[end]))
        {
            end--;
        }
        else
        {
            break;
        }
    }

    if(hasQuote == 1)
    {
        char *newStr = (char*)malloc(sizeof(char) * (end-start)+3);
        memcpy(newStr, str+start, (end-start)+1);
        newStr[(end-start)+1] = '\"';
        newStr[(end-start)+2] = '\0';
        return newStr;
    }
    else
    {
        char *newStr = (char*)malloc(sizeof(char) * (end-start)+2);
        memcpy(newStr, str+start, (end-start)+1);
        newStr[(end-start)+1] = '\0';
        return newStr;
    }
}

char *** shorten(char ***ar)
{
    int i = 0;
    while(ar[i]!=NULL)
    {
        i++;
    }
    char *** temp = (char ***)malloc(sizeof(char**)*i);
    int tempc = 0;
    while(tempc< i)
    {
        temp[tempc] = ar[tempc];
        tempc++;
    }
    //printf("%d",i);
    free(ar);
    return temp;
}


int controller(char *fileName)
{
    int returnValue = -1;
    FILE *fp = fopen(fileName, "r");
    //char *cat[28];
	char *line = NULL;
	size_t len = 246;
	ssize_t x;
    line = (char*)malloc(len*sizeof(char));

	//gets the first line and inserts categories into cat array
	getline(&line, &len, fp);
	char *token = strtok(line, ",");
    //int index = -1;
    int fieldCounter = 0;
    int ze;
	while(token != NULL)
    {
        // cat[fieldCounter] = (char*)malloc(sizeof(char) *strlen(token)+1);
		// strcpy(cat[fieldCounter], token);
        // cat[fieldCounter][strlen(token)] = '\0';
        // if(index == -1)
        // {
        //     // if(strcmp(cat[fieldCounter], checkField) == 0)
        //     // {
        //     //     index = fieldCounter;
        //     //     //printf("%s\t %s", cat[i], fileName);
        //     // }
        // }
        token = strtok(NULL, ",");
		fieldCounter++;
    }
    fclose(fp);
    if(fieldCounter == 28)
        returnValue = 0;
    else
        returnValue = 1;

    return returnValue;
}

void reader(char * fileName, char * checkField, char * fname, char isSpecified, char *outputPath, char * originalDirectory)
{
    //Read the categories
    FILE *fp = fopen(fileName, "r");
    char *cat[28];
	char *line = NULL;
	size_t len = 246;
	ssize_t x;
    line = (char*)malloc(len*sizeof(char));

	//gets the first line and inserts categories into cat array
	getline(&line, &len, fp);
	char *token = strtok(line, ",");
    int index = -1;
    int fieldCounter = 0;
	while(token != NULL)
    {
        cat[fieldCounter] = (char*)malloc(sizeof(char) *strlen(token)+1);
		strcpy(cat[fieldCounter], token);
        cat[fieldCounter][strlen(token)] = '\0';
        if(index == -1)
        {
            if(strcmp(cat[fieldCounter], checkField) == 0)
            {
                index = fieldCounter;
                //printf("%s\t %s", cat[i], fileName);
            }
        }
        token = strtok(NULL, ",");
		fieldCounter++;
    }
    // if no field has been found choose the default.
    if(index == -1)
        index = 0;




    int count = 0;
	int size = 300;
    int arraySize = 0;
	//array where each element of array is ptr to a string array(a record)
	char ***arr = (char***)malloc(sizeof(char**) * size);

	while((x = getline(&line, &len, fp)) != -1)
    {

		//array that holds each value of the record
		char **record =(char**) malloc(sizeof(char*) * 28);

		//if main array reached max capacity then resize it
		if(count == size)
        {
			size *= 2;
			arr = (char***)realloc(arr, size*sizeof(char**));
		}

		//inserts values into record array
		int i = 0;
		while((token = strsep(&line, ",")) != NULL)
        {
			if(token[0] == '\"')
            {
				char *token0 = (char*)malloc(sizeof(char) * strlen(token) +2);
				strcpy(token0, token);
				token0[strlen(token0)] = ',';
				char *token1 = strsep(&line, "\"");
				int t0Length = strlen(token0);
				int t1Length = strlen(token1);
				char *newToken = (char*)malloc(sizeof(char) * (t0Length+t1Length+2));
				strcat(newToken, token0);
				strcat(newToken, token1);

				record[i] = (char*)malloc(sizeof(char) *strlen(newToken)+2);
				char *tempToken = trim(newToken);

				strcpy(record[i], tempToken);

				record[i][strlen(record[i])-2] = '\"';
		                record[i][strlen(record[i])-1] = '\0';

				free(token0);
				free(newToken);
				token1 = strsep(&line, ",");
			}
            else
            {
				char *newToken = trim(token);
                int sz = strlen(newToken)+1;
				record[i] = (char*)malloc(sizeof(char) *sz);
				strcpy(record[i], newToken);
                record[i][sz-1] = '\0';
			}
			i++;
		}
		*(arr+count) = record;
		count++;
	}
    if(count>0 && fieldCounter == 28)
    {

        int scount = 0;
        for(scount = 0; scount < strlen(fname); scount++)
        {
            if(fname[scount] == '.')
                break;
        }
        //printf("%s-> %d",fname,scount);
        int sizeOfWriteString = strlen(fname)-2+6+2+strlen(cat[index])+4;
        char * writeString = (char*)malloc(sizeof(char)*sizeOfWriteString);
        strncpy(writeString, fname, scount);
        strcat(writeString, "-sorted-");
        strcat(writeString, cat[index]);
        strcat(writeString, ".csv");
        writeString[sizeOfWriteString-1] = '\0';

        char *filePath;


        if(isSpecified == '1')
        {
            int supersize = strlen(originalDirectory) + strlen(writeString)+ 2;
            filePath = (char *) malloc(sizeof(char)*supersize);
            strcpy(filePath,originalDirectory);
            filePath[strlen(originalDirectory)]= '/';
            strcat(filePath, writeString);
            filePath[supersize-1] = '\0';
        }
        else
        {
            int supersize = strlen(outputPath) + strlen(writeString)+ 2;
            filePath = (char *) malloc(sizeof(char)*supersize);
            strcpy(filePath,outputPath);
            filePath[strlen(outputPath)] = '/';
            strcat(filePath, writeString);
            filePath[supersize-1] = '\0';
        }

        //printf("%s->%s\n",originalDirectory, filePath );




        arraySize = count;
        arr = shorten(arr);
        char isString = '0';
        if(strcmp(cat[index], "num_critic_for_reviews") == 0 || strcmp(cat[index], "director_facebook_likes") == 0 || strcmp(cat[index], "actor_3_facebook_likes") == 0)
        {
            isString = '1';
        }
        else if(strcmp(cat[index], "actor_1_facebook_likes") == 0 || strcmp(cat[index], "gross") == 0 || strcmp(cat[index], "num_voted_users") == 0)
        {
            isString = '1';
        }
        else if(strcmp(cat[index], "cast_total_facebook_likes") == 0 || strcmp(cat[index], "facenumber_in_poster") == 0 || strcmp(cat[index], "num_user_for_reviews") == 0)
        {
            isString = '1';
        }
        else if(strcmp(cat[index], "budget") == 0 || strcmp(cat[index], "title_year") == 0 || strcmp(cat[index], "actor_2_facebook_likes") == 0)
        {
            isString = '1';
        }
        else if(strcmp(cat[index], "imdb_score") == 0 || strcmp(cat[index], "aspect_ratio") == 0 || strcmp(cat[index], "movie_facebook_likes") == 0)
        {
            isString = '1';
        }

        ms(arr, arraySize, index, isString);
        int q = 0;
        FILE *writer = fopen(filePath, "w");
        for(int k = 0; k< arraySize; k++)
        {
            for(q = 0; q< fieldCounter; q++)
            {
                fprintf(writer, "%s,",arr[k][q]);
                //printf("%s,", arr[k][q]);
            }
            fprintf(writer, "\n");

        }
        fclose(writer);
        //free(writeString);
        //free(filePath);
    }

    free(arr);

}

int isCSV(char *fileName)
{
    const char extension[5] = ".csv";
    char *newExt = strstr(fileName, extension);

    if(newExt == NULL)
    {
        return -1;
    }else
    {
        return 1;
    }
}

static int childProcessCount = 0;
int *pc = &childProcessCount;

void recursiveDir(char *dirName, char *field, char isDefaultOutput, char *outputDir)
{
    //printf("%s\n", dirName);

	static struct dirent *sd;
    DIR *dir = opendir(dirName);

	/*
	reads the initial directory and traverses
	through each file and directory until theres
	no more files/directories
	*/
    pid_t pidD; //pid for directory
    pid_t pidC; //pid for csv
    while((sd = readdir(dir)) != NULL)
    {
        int c = isCSV(sd->d_name);
        //pid_t pidD; //pid for directory
        //pid_t pidC; //pid for csv

        //if directory does not exist
		if(dir == NULL)
        {
            printf("%s is not a directory", sd->d_name);
        }

		//skips over . and .. directory
        int ret = strncmp(sd->d_name, ".", 1);
        if(ret == 0)
        {
            continue;
        }

		//if you find a directory fork it and traverse through it
        if(sd->d_type == DT_DIR)
        {
            pidD = fork();
            //*pc += 1;
			if(pidD == 0)
            {

				char x [strlen(sd->d_name) + strlen(dirName)+2];
				strcpy(x,dirName);
				strcat(x, "/");
				strcat(x,sd->d_name);
				strcat(x, "\0");
				//printf(" --%d-- ",childProcessCount);
				recursiveDir(x, field, isDefaultOutput, outputDir);
                printf("%d, ",getpid());
                //printf("--%d--", *pc);
				exit(0+*pc);
			}
        }

		//if you find a file
		if(sd->d_type == DT_REG)
        {
			if(isCSV(sd->d_name) == 1)
            {

                int size = strlen(sd->d_name)+strlen(dirName)+1+1;
                char *fileDirectory = (char*)malloc(sizeof(char)*(size));
                strcpy(fileDirectory, dirName);
                fileDirectory[strlen(dirName)]= '/';
                strcat(fileDirectory, sd->d_name);
                fileDirectory[size-1] = '\0';



                int checkIfValid = controller(fileDirectory);
                if(checkIfValid == 0)
                {

                    pidC = fork();
                    //*pc += 1;
                    if(pidC == 0)
                    {

                        //
                        //printf("%s-> %s\n", dirName,fileDirectory);
                        reader(fileDirectory, field, sd->d_name, isDefaultOutput, outputDir, dirName );
                        printf("%d, ", getpid());

                        exit(0+*pc);
                    }
                }
                else
                {

                }
                //free(fileDirectory);
			}
		}
		/* prints out everything in the directories */
		//printf("%s\n", sd->d_name);
	}
	closedir(dir);
    int returnStatus;
    waitpid(pidD, &returnStatus, 0);
    waitpid(pidC, &returnStatus, 0);
    //while ((pidC = wait(&status)) > 0);
	//printf("Number of processes: %d\n", childProcessCount);
	//printf("%d, ", getpid());
}









int main(int argc , char *argv[])
{
    char isDefaultOutput = '0';
	char *fieldName = NULL;
	char *dirName = NULL;
	char *outputDir = NULL;


	fieldName = argv[2];
    if(argc == 7)
    {
		dirName = argv[4];
		outputDir = argv[6];

	}
	if(argc == 3)
    {
		dirName = ".";
		outputDir = ".";
        isDefaultOutput = '1';
	}
	if(argc == 5)
    {
		if((strncmp(argv[3],"-d",2)) == 0)
        {
			dirName = argv[4];
			outputDir = ".";
            isDefaultOutput = '1';
		}
        else if((strncmp(argv[3],"-o",2)) == 0)
        {
			dirName = ".";
			outputDir = argv[4];
			isDefaultOutput = '1';
		}
	}
    if(strcmp(outputDir, ".") == 0)
        isDefaultOutput = '1';

    if(isDefaultOutput == '0')
    {
        DIR* dir = opendir("mydir");
        if (dir)
        {
        /* Directory exists. */
            printf("Directory exists");
            closedir(dir);
        }
        else
        {
            mkdir(outputDir, 0700);
        }
    }

	/*
	if(outputDir == NULL){
	    	isDefaultOutput = '1';
	}
	*/

	printf("Initial PID: %d\n", getpid());
	printf("PIDS Of All Child Processes: \n");
	//printf("%s\n", outputDir);
	recursiveDir(dirName, fieldName, isDefaultOutput, outputDir);
    	//reader("m.csv", fieldName);
    printf("\n");
	printf("Number of processes: %d\n", *pc);


	return 0;
}

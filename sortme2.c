#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include "mergesort.h"

static char *dirName = NULL;
static char *outputDir = NULL;
static char *fieldName = NULL;
static char isChar = 'a';
int fieldIndex = 0;
pthread_mutex_t lock;
pthread_mutex_t lockArr;

char ***globalArr = NULL;
int globalSize = 0;
int count = 0;

struct tidNode{
    pthread_t tid;
    struct tidNode *next;
};

char *trim(char *str)
{
    int start = 0;
    int end = strlen(str)-1;
    int hasQuote = 0;
    if(str[strlen(str)-1] == '\"')
    {
        hasQuote = 1;
        end--;
    }
    while(start < strlen(str))
    {
        if(isspace(str[start]))
        {
            start++;
        }
        else
        {
            break;
        }
    }
    if(start == strlen(str))
    {
        return str;
    }
    while(end>0)
    {
        if(isspace(str[end]))
        {
            end--;
        }
        else
        {
            break;
        }
    }

    if(hasQuote == 1)
    {
        char *newStr = (char*)malloc(sizeof(char) * (end-start)+3);
        memcpy(newStr, str+start, (end-start)+1);
        newStr[(end-start)+1] = '\"';
        newStr[(end-start)+2] = '\0';
        return newStr;
    }
    else
    {
        char *newStr = (char*)malloc(sizeof(char) * (end-start)+2);
        memcpy(newStr, str+start, (end-start)+1);
        newStr[(end-start)+1] = '\0';
        return newStr;
    }
}
char *** shorten(char ***ar)
{
    int i = 0;
    while(ar[i]!=NULL)
    {
        i++;
    }
    char *** temp = (char ***)malloc(sizeof(char**)*i);
    int tempc = 0;
    while(tempc< i)
    {
        temp[tempc] = ar[tempc];
        tempc++;
    }
    //printf("%d",i);
    free(ar);
    return temp;
}


struct tidNode *head = NULL;

void addToFront(pthread_t tid){
    struct tidNode *temp = (struct tidNode*) malloc(sizeof(struct tidNode));
    temp->tid = tid;
    temp->next = head;
    pthread_mutex_lock(&lock);
    head = temp;
    pthread_mutex_unlock(&lock);
};

int isCSV(char *fileName){
    const char extension[5] = ".csv";
    char *newExt = strstr(fileName, extension);

    if(newExt == NULL)
    {
        return -1;
    }else
    {
        return 1;
    }

}

void *ms(){
    return 0;
}

int csvController(char *fileName)
{
    int returnValue = -1;
    pthread_mutex_lock(&lock);
    FILE *fp = fopen(fileName, "r");
    char *line = NULL;
    size_t len = 246;
    ssize_t x;
    line = (char*)malloc(len*sizeof(char));

    //gets the first line and inserts categories into cat array
    getline(&line, &len, fp);
    char *token = strtok(line, ",");
    //int index = -1;
    int fieldCounter = 0;
    int ze;
    while(token != NULL)
    {

        token = strtok(NULL, ",");
        fieldCounter++;
    }
    fclose(fp);
    pthread_mutex_unlock(&lock);

    free(line);
    if(fieldCounter == 28)
        returnValue = 0;
    else
        returnValue = 1;

    return returnValue;
}

void reader(char * fileName)
{
    FILE *fp = fopen(fileName, "r");
    char *cat[28];
    char *line = NULL;
    size_t len = 246;
    ssize_t x;
    line = (char*)malloc(len*sizeof(char));

    //gets the first line and inserts categories into cat array
    getline(&line, &len, fp);
    char *token = strtok(line, ",");
    int index = -1;
    int fieldCounter = 0;
    while(token != NULL)
    {
        cat[fieldCounter] = (char*)malloc(sizeof(char) *strlen(token)+1);
        strcpy(cat[fieldCounter], token);
        cat[fieldCounter][strlen(token)] = '\0';
        if(index == -1)
        {
            if(strcmp(cat[fieldCounter], fieldName) == 0)
            {
                index = fieldCounter;
                //printf("%s\t %s", cat[i], fileName);
            }
        }
        token = strtok(NULL, ",");
        fieldCounter++;
    }


    // if no field has been found choose the default.
    if(index == -1)
        index = 0;




    int count = 0;
	int size = 300;
    int arraySize = 0;
	//array where each element of array is ptr to a string array(a record)
	char ***arr = (char***)malloc(sizeof(char**) * size);

	while((x = getline(&line, &len, fp)) != -1)
    {

		//array that holds each value of the record
		char **record =(char**) malloc(sizeof(char*) * 28);

		//if main array reached max capacity then resize it
		if(count == size)
        {
			size *= 2;
			arr = (char***)realloc(arr, size*sizeof(char**));
		}

		//inserts values into record array
		int i = 0;
		while((token = strsep(&line, ",")) != NULL)
        {
			if(token[0] == '\"')
            {
				char *token0 = (char*)malloc(sizeof(char) * strlen(token) +2);
				strcpy(token0, token);
				token0[strlen(token0)] = ',';
				char *token1 = strsep(&line, "\"");
				int t0Length = strlen(token0);
				int t1Length = strlen(token1);
				char *newToken = (char*)malloc(sizeof(char) * (t0Length+t1Length+2));
				strcat(newToken, token0);
				strcat(newToken, token1);

				record[i] = (char*)malloc(sizeof(char) *strlen(newToken)+2);
				char *tempToken = trim(newToken);

				strcpy(record[i], tempToken);

				record[i][strlen(record[i])-2] = '\"';
		                record[i][strlen(record[i])-1] = '\0';

				free(token0);
				free(newToken);
				token1 = strsep(&line, ",");
			}
            else
            {
				char *newToken = trim(token);
                int sz = strlen(newToken)+1;
				record[i] = (char*)malloc(sizeof(char) *sz);
				strcpy(record[i], newToken);
                record[i][sz-1] = '\0';

			}
			i++;
		}
		*(arr+count) = record;

		count++;
	}
    fclose(fp);
    if(count>0 && fieldCounter == 28)
    {
        arraySize = count;
        arr = shorten(arr);
        char isString = '0';
        if(strcmp(cat[index], "num_critic_for_reviews") == 0 || strcmp(cat[index], "director_facebook_likes") == 0 || strcmp(cat[index], "actor_3_facebook_likes") == 0)
        {
            isString = '1';
        }
        else if(strcmp(cat[index], "actor_1_facebook_likes") == 0 || strcmp(cat[index], "gross") == 0 || strcmp(cat[index], "num_voted_users") == 0)
        {
            isString = '1';
        }
        else if(strcmp(cat[index], "cast_total_facebook_likes") == 0 || strcmp(cat[index], "facenumber_in_poster") == 0 || strcmp(cat[index], "num_user_for_reviews") == 0)
        {
            isString = '1';
        }
        else if(strcmp(cat[index], "budget") == 0 || strcmp(cat[index], "title_year") == 0 || strcmp(cat[index], "actor_2_facebook_likes") == 0)
        {
            isString = '1';
        }
        else if(strcmp(cat[index], "imdb_score") == 0 || strcmp(cat[index], "aspect_ratio") == 0 || strcmp(cat[index], "movie_facebook_likes") == 0)
        {
            isString = '1';
        }
        msort(arr, arraySize, index, isString);
        if(isChar == 'a')
        {
            isChar = isString;
            fieldIndex = index;
        }
        for(int i = 0; i< arraySize; i++)
        {
            //printf("%s\n", arr[i][index]);
        }
    }
    if(arr != NULL)
    {
        pthread_mutex_lock(&lockArr);
        if(arraySize != 0)
        {
            printf("arr size of file: %d\n", arraySize);
            if(globalArr != NULL)
            {
                globalArr = (char ***) realloc(globalArr, (globalSize+arraySize)*sizeof(char **));
                int acount = 0;
                for(acount = 0; acount < arraySize; acount++)
                {
                    globalArr[globalSize+ acount] = arr[acount];
                }
                globalSize += acount;
                printf("%d\n", globalSize);
            }
            else
            {
                globalArr = arr;
                globalSize = arraySize;
            }
        }


        pthread_mutex_unlock(&lockArr);
    }


}


void *recursiveDir(void *dname){
    DIR *dir;
    struct dirent *sd;
    int ret;
    dir = opendir(dname);
    while((sd = readdir(dir)) != NULL)
    {
        ret = strncmp(&sd->d_name[0],".",1);
        if(ret == 0){
            continue;
        }
        if(sd->d_type == DT_DIR)
        {
            char *new_str;
            if((new_str = malloc(strlen(dname)+strlen(sd->d_name)+2)) != NULL)
            {
                new_str[0] = '\0';   // ensures the memory is an empty string
                strcat(new_str,dname);
                strcat(new_str, "/");
                strcat(new_str,sd->d_name);
                pthread_t tid;
                pthread_create(&tid, NULL, recursiveDir, (void*)new_str);
                count++;
                addToFront(tid);
                printf(" %d \n", (int)tid);
            }
        }
        else if(sd->d_type == DT_REG)
        {
            if(isCSV(sd->d_name) == 1)
            {
                //printf("%s\t%s\n",dname, sd->d_name);


                int size = strlen(sd->d_name)+strlen(dname)+1+1;
                char *fileDirectory = (char*)malloc(sizeof(char)*(size));
                strcpy(fileDirectory, dname);
                fileDirectory[strlen(dname)]= '/';
                strcat(fileDirectory, sd->d_name);
                fileDirectory[size-1] = '\0';
                int px = csvController(fileDirectory);
                    if(px == 0){
                        pthread_t tidC;
                        pthread_create(&tidC, NULL, ms, NULL);
                        count++;
                        addToFront(tidC);
                        printf(" %d \n", (int)tidC);
                        reader(fileDirectory);

                }
            }
        }
        //printf("%s\n", sd->d_name);
    }
    closedir(dir);

    return 0;
}

int main(int argc, char *argv[])
{

    pthread_mutex_init(&lock, NULL);
    pthread_mutex_init(&lockArr, NULL);

    int i = 1;
    if(argc == 7){
        while(i < 7){
            if((strncmp(argv[i], "-c", 2)) == 0){
                fieldName = argv[i+1];
            }
            if((strncmp(argv[i], "-d", 2)) == 0){
                dirName = argv[i+1];
            }
            if((strncmp(argv[i], "-o", 2)) == 0){
                outputDir = argv[i+1];
            }
            i++;
        }
    }
    if(argc == 3)
    {
        fieldName = argv[2];
        dirName = ".";
        outputDir = ".";
    }
    if(argc == 5)
    {
        while(i < 5){
            if((strncmp(argv[i], "-c", 2)) == 0)
            {
                fieldName = argv[i+1];
            }
            if((strncmp(argv[i], "-d", 2)) == 0)
            {
                dirName = argv[i+1];
                outputDir = ".";
            }
            if((strncmp(argv[i], "-o", 2)) == 0)
            {
                outputDir = argv[i+1];
                dirName = ".";
            }
            i++;
        }
    }
    //printf("Initial PID: %d\n", getpid());
    //printf("TIDs Of All Child Threads: ");
    pthread_t tid123;
    pthread_create(&tid123, NULL, recursiveDir, (void*)dirName);
    pthread_join(tid123, NULL);

    struct tidNode *ptr = head;
    while(ptr != NULL){
        pthread_join(ptr->tid, NULL);
        ptr = ptr->next;
    }


    printf("\n");
    printf("Count is %d\n", count);
    msort(globalArr, globalSize, fieldIndex, isChar);
    for(int k = 0; k< globalSize; k++)
    {
        printf("%s\n", globalArr[k][fieldIndex]);
    }
    return 0;
}

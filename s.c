#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "mergesort.h"
#include <dirent.h>
#include <sys/types.h>

int arraySize = 0;

char *trim(char *str){
        int start = 0;
        int end = strlen(str)-1;
        int hasQuote = 0;
        if(str[strlen(str)-1] == '\"'){
                hasQuote = 1;
                end--;
        }
        while(start < strlen(str)){
                if(isspace(str[start])){
                        start++;
                }else{
                        break;
                }
        }
        if(start == strlen(str)){
                return str;
        }
        while(end>0){
                if(isspace(str[end])){
                        end--;
                }else{
                        break;
                }
        }

        if(hasQuote == 1){
                char *newStr = (char*)malloc(sizeof(char) * (end-start)+3);
                memcpy(newStr, str+start, (end-start)+1);
                newStr[(end-start)+1] = '\"';
                newStr[(end-start)+2] = '\0';
                return newStr;
        }else{
                char *newStr = (char*)malloc(sizeof(char) * (end-start)+2);
                memcpy(newStr, str+start, (end-start)+1);
                newStr[(end-start)+1] = '\0';
                return newStr;
        }
}

char *** shorten(char ***ar)
{
    int i = 0;
    while(ar[i]!=NULL)
    {
        i++;
    }
    char *** temp = (char ***)malloc(sizeof(char**)*i);
    int tempc = 0;
    while(tempc< i)
    {
        temp[tempc] = ar[tempc];
        tempc++;
    }
    //printf("%d",i);
    arraySize = i;
    free(ar);
    return temp;
}


int main(int argc, char *argv[]){

	//string array of categories
	char *cat[28];
	char *line = NULL;
	size_t len = 246;
	ssize_t x;
        line = (char*)malloc(len*sizeof(char));

	//gets the first line and inserts categories into cat array
	getline(&line, &len, stdin);
	char *token = strtok(line, ",");
    int index = -1;
    int i = 0;
	while(token != NULL)
    {
        cat[i] = (char*)malloc(sizeof(char) *strlen(token)+1);
		strcpy(cat[i], token);
        if(index == -1)
        {
            if(strcmp(cat[i], argv[2]) == 0)
            {
                index = i;
            }
        }
        token = strtok(NULL, ",");
		i++;
    }


	int count = 0;
	int size = 300;

	//array where each element of array is ptr to a string array(a record)
	char ***arr = (char***)malloc(sizeof(char**) * size);

	while((x = getline(&line, &len, stdin)) != -1)
    {

		//array that holds each value of the record
		char **record =(char**) malloc(sizeof(char*) * 28);

		//if main array reached max capacity then resize it
		if(count == size)
        {
			size *= 2;
			arr = (char***)realloc(arr, size*sizeof(char**));
		}

		//inserts values into record array
		int i = 0;
		while((token = strsep(&line, ",")) != NULL)
        {
			if(token[0] == '\"')
            {
				char *token0 = (char*)malloc(sizeof(char) * strlen(token) +2);
				strcpy(token0, token);
				token0[strlen(token0)] = ',';
				char *token1 = strsep(&line, "\"");
				int t0Length = strlen(token0);
				int t1Length = strlen(token1);
				char *newToken = (char*)malloc(sizeof(char) * (t0Length+t1Length+2));
				strcat(newToken, token0);
				strcat(newToken, token1);

				record[i] = (char*)malloc(sizeof(char) *strlen(newToken)+2);
				char *tempToken = trim(newToken);

				strcpy(record[i], tempToken);

				record[i][strlen(record[i])-2] = '\"';
                record[i][strlen(record[i])-1] = '\0';

				free(token0);
				free(newToken);
				token1 = strsep(&line, ",");
			}
            else
            {
				char *newToken = trim(token);
				record[i] = (char*)malloc(sizeof(char) *strlen(newToken)+1);
				strcpy(record[i], newToken);
			}
			i++;
		}
		*(arr+count) = record;
		count++;
	}
    arraySize = size;
    arr = shorten(arr);

    ms(arr, arraySize, index);
    int q = 0;

    for(int k = 0; k< arraySize; k++)
    {
        printf("%s\n", arr[k][index]);
    }
    // for(q = 0; q< arraySize; q++)
    // {
    //     int col = 0;
    //     for(col = 0; col< 28; col++)
    //     {
    //         printf("%s,", arr[q][col]);
    //     }
    //      printf("\n");
    // }
    return 0;
}
